var app = angular.module('jiratime', []);

// This only works if you
// * install
// https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en
// * enable it
// * configure it to intercept https://apprenda.atlassian.net/rest/api/*
app.config(function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
});

app.controller('MainCtrl', function($scope, $http, $q) {
    $scope.rootUrl = "https://apprenda.atlassian.net";

    var method = "GET";

    var storageKey = "jiraTimeCred";

    $scope.userId = "";
    $scope.password = "";
    
    var credential = localStorage.getItem(storageKey);
    if (credential != null) {
	console.log("Got credential from local storage");
	var parts = atob(credential).split(":");
	$scope.userId = parts[0];
	$scope.password = parts[1]
	$scope.remember = true;
    }

    $scope.submit = function() {
	credential = btoa($scope.userId + ":" + $scope.password);
	
	if ($scope.remember) {
	    console.log("Setting local storage");
	    localStorage.setItem(storageKey, credential);
	}
	else {
	    console.log("Clearing local storage");
	    localStorage.removeItem(storageKey);
	}

	$scope.getWork();
	
    };

    // Returns a promise.  When that promise is satisfied, the data
    // passed back is a list of keys of recent tickets.
    $scope.getRecentTickets = function(){
        var deferred = $q.defer();
	
        $http({
            url: $scope.rootUrl+"/rest/api/2/search?jql=filter=28203",
            method: method,
            headers: {
                "Authorization": "Basic " + credential
            }
        })
	    .then(function successCallback(response) {
		var keys = [];
		angular.forEach(response.data.issues, function(issue,index) {
		    keys.push(issue.key);
		});
		deferred.resolve(keys);
		
	    }, function errorCallback(response) {
		console.log("Error");
		deferred.reject(response);
	    });

	return deferred.promise;
    };

    $scope.scales = [ "day", "week", "month" ];
    $scope.scale = "day";

    // Compute start of period (day, week, month) from reference date,
    // typically now.
    //
    // @param reference a Date object
    // @return a date object at the start of the period
    var startOfPeriod = function(reference, scale) {
	var start = reference;

	// We always want to start at midnight so we always zero out the
	// time of day components
	start.setHours(0);
	start.setMinutes(0);
	start.setSeconds(0);
	
	switch (scale) {
	case 'week':
	    // There is no setDay() so we have to get today's day-of-week
	    // and subtract enough time to get to the start of the week
	    var ms = start.getTime();
	    ms -= start.getDay() * 24 * 60 * 60 * 1000;
	    start.setTime(ms);
	    break;
	    
	case 'month':
	    // Set day of month to 1 and we're done
	    start.setDate(1)
	    break;
	}
	
	return start;
    };

    $scope.getTicketWork = function(key) {
        $http({
            url: $scope.rootUrl+"/rest/api/2/issue/"+key+"/worklog",
            method: method,
            headers: {
                "Authorization": "Basic " + credential
            }
        })
	    .then(function successCallback(response) {
		var sop = startOfPeriod(new Date(Date.now()), $scope.scale);
		
		var worklogs = response.data.worklogs;

		angular.forEach(worklogs, function(worklog, index) {
		    if (worklog.author.emailAddress == $scope.userId) {
			var ms = Date.parse(worklog.started);
			var s = new Date(ms);
			if (s >= sop) {
			    var secondsSpent = parseInt(worklog.timeSpentSeconds);
			    $scope.totalSeconds += secondsSpent;
			    $scope.totalHours = ($scope.totalSeconds/3600.0).toFixed(2);
			    var e = new Date(ms+(secondsSpent*1000));

			    worklog.key = key;
			    worklog.uiStart = s;
			    worklog.uiEnd = e;
			    
			    $scope.work.push(worklog);
			}
		    }
		});
	    }, function errorCallback(response) {
		console.log("Error");
	    });
    };

    $scope.getWork = function() {
	// An array of
	// {
	//    "ticket" : ticketKey,
	//    "start" : time,
	//    "duration": ..
	// }
	$scope.work = []

	$scope.totalSeconds = 0;

	$scope.getRecentTickets()
	    .then(function successCallback(keys) {
		angular.forEach(keys, function(key, index) {
		    $scope.getTicketWork(key);
		});
	    }, function errorCallback(response) {
		console.log("Error!");
		//FIXME - if we get a 401, remove the stored credentials
		console.log(response);
	    });
    };

    $scope.getWork();
});
